/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 */


#include "processesoverview.hpp"
#include <QApplication>

#include "processqueue.hpp"

//#include <iostream> // Used in testing only

/**
 * @brief The Main class and function.
 * ITRW 316
 * main.cpp
 * Purpose: Program entry point.
 */

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	ProcessesOverview w;
	w.show();

	return a.exec();

	////////PROCESS QUEUE TESTING////////
//	std::cout << "MAIN TEST" << std::endl;

//	Process *p1 = new Process('A', 7);
//	Process *p2 = new Process('B', 4);
//	Process *p3 = new Process('C', 5);
//	Process *p4 = new Process('D', 2);
//	Process *p5 = new Process('E', 6);

//	ProcessQueue *pq = new ProcessQueue(p1);
//	pq->enqueue_process(p2);
//	pq->enqueue_process(p3);
//	pq->enqueue_process(p4);
//	pq->enqueue_process(p5);

//	pq->list_processes();

//	std::cout << "HEAD: " << (char)pq->get_first()->get_id() << std::endl;
//	std::cout << "TAIL: " << (char)pq->get_last()->get_id() << std::endl;

////	std::cout << (char)p2->get_id() << "->next: " << (char)p2->get_next_process()->get_id() << std::endl;
////	std::cout << (char)p3->get_id() << "->prev: " << (char)p3->get_previous_process()->get_id() << std::endl;

////	std::cout << (char)p2->get_id() << "->next: " << (char)p2->get_next_process()->get_id() << std::endl;
////	std::cout << (char)p2->get_id() << "->prev: " << (char)p2->get_previous_process()->get_id() << std::endl;

//	std::cout << "Deleting process " << (char)p2->get_id() << std::endl;
//	pq->remove_process(p2);
//	pq->list_processes();

//	std::cout << (char)p1->get_id() << "->next: " << (char)p1->get_next_process()->get_id() << std::endl;
//	std::cout << (char)p3->get_id() << "->prev: " << (char)p3->get_previous_process()->get_id() << std::endl;

//	std::cout << "HEAD: " << (char)pq->get_first()->get_id() << std::endl;
//	std::cout << "TAIL: " << (char)pq->get_last()->get_id() << std::endl;

////	pq->remove_process(p5);
////	pq->list_processes();

////	std::cout << (char)p4->get_id() << "->next: " << (char)p4->get_next_process()->get_id() << std::endl;
////	std::cout << (char)p1->get_id() << "->prev: " << (char)p1->get_previous_process()->get_id() << std::endl;
////	std::cout << "TAIL: " << (char)pq->get_last()->get_id() << std::endl;

//	std::cout << "Deleting process " << (char)p1->get_id() << std::endl;
//	pq->remove_process(p1);
//	pq->list_processes();

//	std::cout << (char)p5->get_id() << "->next: " << (char)p5->get_next_process()->get_id() << std::endl;
//	std::cout << (char)p3->get_id() << "->prev: " << (char)p3->get_previous_process()->get_id() << std::endl;
//	std::cout << "HEAD: " << (char)pq->get_first()->get_id() << std::endl;


//	delete pq;

//	return 0;
}
