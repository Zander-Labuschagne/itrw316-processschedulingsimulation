/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 *
 * Purpose: Implementation of the Process class declared in process.hpp. @see process.hpp
 * ITRW 316
 * process.cpp
 */

#include "process.hpp"

/**
 * @brief Process::Process the default constructor
 */
Process::Process()
{
	Process(0, 0);
}

/**
 * @brief Process::Process the overloaded conctructor
 * @param id denotes process id for identification
 * @param cycles denotes number of cycles to execute or cycle lifetime or process length etc.
 * @param priority denotes priority of this process used only in priority scheduling
 * @param prev is a pointer/reference to the previous process
 * @param next is a pointer/reference to the next process
 * @see process.hpp documentation for descriptions on class member variables
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process::Process(unsigned short id, unsigned int cycles, unsigned short priority, Process *prev, Process *next)
{
	this->id = id;
	this->cycles_total = cycles;
	this->set_priority(priority);
	this->cycles_completed = 0;
	this->completed = false;
	this->set_next_process(next);
	this->set_previous_process(prev);
}

/**
 * @brief Process::get_id
 * @return id of the process
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned short Process::get_id()
{
	return this->id;
}

/**
 * @brief Process::get_priority
 * @return the priority of the process
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned short Process::get_priority()
{
	return this->priority;
}

/**
 * @brief Process::set_priority
 * @param priority for thius process
 * @since 1.0
 * @version 1.0 03/03/19
 */
void Process::set_priority(unsigned short priority)
{
	this->priority = priority;
}

/**
 * @brief Process::tick
 * Executes a cycle of this process
 * @since 1.0
 * @version 1.0 03/03/19
 */
void Process::tick()
{
	++this->cycles_completed;
	if (this->get_cycles_total() == this->get_cycles_completed())
		this->completed = true;
}

/**
 * @brief Process::is_completed
 * @return whether thus process has completed or not
 * @since 1.0
 * @version 1.0 03/03/19
 */
bool Process::is_completed()
{
	return this->completed;
}

/**
 * @brief Process::get_cycles_completed
 * @return number of cycles completed
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned int Process::get_cycles_completed()
{
	return this->cycles_completed;
}

/**
 * @brief Process::get_cycles_total
 * @return total cycles to execute for this process
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned int Process::get_cycles_total()
{
	return this->cycles_total;
}

/**
 * @brief Process::get_cycles_remaining
 * @return cycles yet to execute for this process.
 * Very handy in Shortest Job First and Shortest Remaining Time algorithms
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned int Process::get_cycles_remaining()
{
	return this->get_cycles_total() - this->get_cycles_completed();
}

/**
 * @brief Process::get_progress
 * @return the progress of this process.
 * Currently not used, just thought it could be usefull somewhere sometime.
 * @since 1.0
 * @version 1.0 03/03/19
 */
float Process::get_progress()
{
	return this->get_cycles_completed() / this->get_cycles_total();
}

/**
 * @brief Process::set_next_process
 * @param process to follow the current
 * @since 1.0
 * @version 1.0 03/03/19
 */
void Process::set_next_process(Process *process)
{
	this->next = process;
}

/**
 * @brief Process::get_next_process
 * @return the process following the current
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process* Process::get_next_process()
{
	return this->next;
}

/**
 * @brief Process::set_previous_process
 * @param process to place to proceed the current
 * @since 1.0
 * @version 1.0 03/03/19
 */
void Process::set_previous_process(Process *process)
{
	this->previous = process;
}

/**
 * @brief Process::get_previous_process
 * @return the process preceeding the current
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process* Process::get_previous_process()
{
	return this->previous;
}

