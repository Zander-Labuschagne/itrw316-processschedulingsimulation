/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 */

#ifndef PROCESS_H
#define PROCESS_H

/**
 * @brief The Process class
 * ITRW 316
 * process.hpp
 * Purpose: Represents a process containing the following members along with their accessor and mutator functions where necesarry:
 * id: An integer used to identify each process uniquely.
 * priority: Used in priority scheduling only, to prioritize each process as given in input.
 * cycles_total: Denotes the cycle life time of the process. (Could be interpreted as the length of the process).
 * cycles_completed: Denotes the number of cycles executed or completed. The process is done if cycles_completed == cycles_total.
 * completed: Returns a boolean value if the process has completed successfully.
 * next: A pointer/reference to the next process. Easier traversal of processes especially in the case of the round robin algorithm.
 * previous: A pointer/reference to the previous process. Keeping track of the previous process aids in some helper functions to avoid extra complexity.
 */
class Process
{
public:
	Process();
	Process(unsigned short id, unsigned int cycles, unsigned short priority = 0, Process *prev = 0, Process *next = 0);
	void tick();
	bool is_completed();
	unsigned short get_id();
	unsigned short get_priority();
	void set_priority(unsigned short priority);
	unsigned int get_cycles_completed();
	unsigned int get_cycles_total();
	unsigned int get_cycles_remaining();
	float get_progress();
	Process *get_next_process();
	void set_next_process(Process*);
	void set_previous_process(Process*);
	Process *get_previous_process();

private:
	unsigned short id;
	unsigned short priority;
	unsigned int cycles_total;
	unsigned int cycles_completed;
	bool completed;
	Process *next;
	Process *previous;
};

#endif // PROCESS_H
