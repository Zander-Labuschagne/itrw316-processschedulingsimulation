/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 *
 * Purpose: Implementation of the ProcessOverview class declared in processoverview.hpp. @see processoverview.hpp
 * This is the main GUI window
 * ITRW 316
 * processoverview.cpp
 */

#include "processesoverview.hpp"

#include <QString>
#include <QCoreApplication>
#include <QThread>

#include <iostream> // Used in testing

/**
 * @brief ProcessesOverview::ProcessesOverview the overloaded constructor, or my init function for GUI and backend stuff
 * @param parent of main window, defaults to 0 or null pointer as it is the parent of all
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessesOverview::ProcessesOverview(QWidget *parent) : QMainWindow(parent)
{
	// init
	this->preliminary_process_count = 0;
	this->preliminary_queue_count = 0;
	this->active_processes = new ProcessQueue();
	this->setFixedSize(1280, 780);
	std::cout << SCREEN_WIDTH << "x" << SCREEN_HEIGHT << std::endl;

	// GUI
	// Choose algorithm
	cmb_algorithm = new QComboBox(this);
	cmb_algorithm->move(375, 50);
#ifdef __APPLE__
	cmb_algorithm->setFixedSize(200, 35);
#elif
	cmb_algorithm->setFixedSize(180, 25);
#endif
	cmb_algorithm->addItem("First Come First Serve");
	cmb_algorithm->addItem("Shortest Job First");
	cmb_algorithm->addItem("Shortest Remaining Time");
	cmb_algorithm->addItem("Round Robin");
	cmb_algorithm->addItem("Priority");
	cmb_algorithm->addItem("Multiple Queues");
	cmb_algorithm->setCurrentIndex(0);
	cmb_algorithm->show();
	connect(cmb_algorithm, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(algorithm_change(const QString&)));


	// Headings
	lbl_cycles = new QLabel(this);
	lbl_cycles->move(117, this->Y_OFFSET - 25);
	lbl_cycles->setFixedSize(60, this->COMPONENT_HEIGHT);
	lbl_cycles->setText("CYCLES");

	lbl_priority = new QLabel(this);
	lbl_priority->move(181, this->Y_OFFSET - 25);
	lbl_priority->setFixedSize(60, this->COMPONENT_HEIGHT);
	lbl_priority->setText("PRIORITY");

	// Push Buttons
	btn_add_process = new QPushButton(this);
	btn_add_process->setText(" + ");
#ifdef __APPLE__
	btn_add_process->setFixedSize(35, 30);
#elif
	btn_add_process->setFixedSize(45, this->BOTTOM_BUTTON_HEIGHT);
#endif
	connect(btn_add_process, SIGNAL(clicked()), this, SLOT(add_process()));

	// Simulation Action/Control push buttons
	btn_start = new QPushButton(this);
	btn_start->setText("Start Simulation");
	btn_start->setFixedSize(this->BOTTOM_BUTTON_WIDTH, this->BOTTOM_BUTTON_HEIGHT);
	connect(btn_start, SIGNAL(clicked()), this, SLOT(start()));

	btn_pause = new QPushButton(this);
	btn_pause->setText("Pause Simulation");
	btn_pause->setFixedSize(this->BOTTOM_BUTTON_WIDTH, this->BOTTOM_BUTTON_HEIGHT);
	btn_pause->setEnabled(false);
	connect(btn_pause, SIGNAL(clicked()), this, SLOT(pause()));

	btn_reset = new QPushButton(this);
	btn_reset->setText("Reset Simulation");
	btn_reset->setFixedSize(this->BOTTOM_BUTTON_WIDTH, this->BOTTOM_BUTTON_HEIGHT);
	btn_reset->setEnabled(false);
	connect(btn_reset, SIGNAL(clicked()), this, SLOT(reset()));

	btn_exit = new QPushButton(this);
	btn_exit->setText("Exit");
	btn_exit->setFixedSize(this->BOTTOM_BUTTON_WIDTH, this->BOTTOM_BUTTON_HEIGHT);
	connect(btn_exit, SIGNAL(clicked()), this, SLOT(exit()));

	//MQ Section
	grp_mq = new QGroupBox(this);
	grp_mq->move(930, this->Y_OFFSET - 25);
	grp_mq->setFixedSize(320, 380);
	grp_mq->setTitle("Multiple Queues Section");

	lbl_tq = new QLabel(grp_mq);
	lbl_tq->setFixedSize(60, this->COMPONENT_HEIGHT);
	lbl_tq->setText("TQ");

	btn_add_queue = new QPushButton(grp_mq);
	btn_add_queue->setText(" + ");
#ifdef __APPLE__
	btn_add_queue->setFixedSize(25, 22);
#elif
	btn_add_queue->setFixedSize(45, this->BOTTOM_BUTTON_HEIGHT);
#endif
	connect(btn_add_queue, SIGNAL(clicked()), this, SLOT(add_queue()));

	btn_submit_mq = new QPushButton(grp_mq);
	btn_submit_mq->setText("Submit MQ Setup");
#ifdef __APPLE__
	btn_submit_mq->setFixedSize(150, this->BOTTOM_BUTTON_HEIGHT);
#elif
	btn_submit_mq->setFixedSize(130, this->BOTTOM_BUTTON_HEIGHT);
#endif
	connect(btn_submit_mq, SIGNAL(clicked()), this, SLOT(submit_mq()));
	grp_mq->setVisible(false);

	// post init
	add_queue();

	add_process();
	add_process();

	// Timers, each algorithm has its own
	tmr_fcfs = new QTimer(this);
	connect(tmr_fcfs, SIGNAL(timeout()), this, SLOT(fcfs()));
	tmr_fcfs->setInterval(this->TIMER_INTERVAL);

	tmr_sjf = new QTimer(this);
	connect(tmr_sjf, SIGNAL(timeout()), this, SLOT(sjf()));
	tmr_sjf->setInterval(this->TIMER_INTERVAL);

	tmr_srt = new QTimer(this);
	connect(tmr_srt, SIGNAL(timeout()), this, SLOT(srt()));
	tmr_srt->setInterval(this->TIMER_INTERVAL);

	tmr_rr = new QTimer(this);
	connect(tmr_rr, SIGNAL(timeout()), this, SLOT(rr()));
	tmr_rr->setInterval(this->TIMER_INTERVAL);

	tmr_pr = new QTimer(this);
	connect(tmr_pr, SIGNAL(timeout()), this, SLOT(pr()));
	tmr_pr->setInterval(this->TIMER_INTERVAL);

	tmr_mq = new QTimer(this);
	connect(tmr_mq, SIGNAL(timeout()), this, SLOT(mq()));
	tmr_mq->setInterval(this->TIMER_INTERVAL);

	tmr_mqq = new QTimer(this);
	connect(tmr_mqq, SIGNAL(timeout()), this, SLOT(mqq()));
	tmr_mqq->setInterval(this->TIMER_INTERVAL);
}

/**
 * @brief ProcessesOverview::algorithm_change executes every time algorithm is changed to detect the selection of MQ which needs an additional GUI section
 * @param algorithm chosen
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::algorithm_change(const QString &algorithm)
{
	if (algorithm == "Multiple Queues")
		grp_mq->setVisible(true);
	else
		grp_mq->setVisible(false);
}

/**
 * @brief ProcessesOverview::refresh_bottom_buttons for each time a process is added the buttons below must move down to make space for the list of processes. @see add_proces()
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::refresh_bottom_buttons()
{
	btn_add_process->move(441, this->BOTTOM_BUTTONS_Y_OFFSET - 40 + (this->Y_PADDING * this->preliminary_process_count));
	btn_add_process->show();

	btn_start->move(210, this->BOTTOM_BUTTONS_Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	btn_start->show();

	btn_pause->move(340, this->BOTTOM_BUTTONS_Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	btn_pause->show();

	btn_reset->move(470, this->BOTTOM_BUTTONS_Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	btn_reset->show();

	btn_exit->move(600, this->BOTTOM_BUTTONS_Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	btn_exit->show();
}

/**
 * @brief ProcessesOverview::refresh_mq_buttons similar to @see refresh_bottom_buttons() but for the MQ section
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::refresh_mq_buttons()
{
#ifdef __APPLE__
	btn_add_queue->move(7, this->MQ_Y_OFFSET + (this->Y_PADDING * this->preliminary_queue_count) + 5);
#elif
	btn_add_queue->move(10, this->MQ_Y_OFFSET + (this->Y_PADDING * this->preliminary_queue_count));
#endif
	btn_add_queue->show();

	btn_submit_mq->move(60, this->MQ_Y_OFFSET + (this->Y_PADDING * this->preliminary_queue_count));
	btn_submit_mq->show();

	lbl_tq->move(120, 35);
	lbl_tq->show();
}

/**
 * @brief ProcessesOverview::add_process adds a line of GUI components for another process to be added, BUT preliminary, process is NOT yet queued or in the READY pool. It needs to be activated after inputs are given for it to become READY
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::add_process()
{
	QLabel *lbl_process = new QLabel(this);
	lbl_process->move(30, this->Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	lbl_process->setFixedSize(60, this->COMPONENT_HEIGHT);
	QString title = QString("Process ") + (65 + this->preliminary_process_count);
	lbl_process->setText(title);
	lbl_process->show();
	lst_lbl_processes.append(lbl_process);

	QSpinBox *spn_cycles = new QSpinBox(this);
	spn_cycles->move(110, this->Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	spn_cycles->setFixedSize(60, this->COMPONENT_HEIGHT);
	spn_cycles->setObjectName(QString("spn_") + (65 + this->preliminary_process_count));
	spn_cycles->show();
	spn_cycles->setMinimum(1);
	lst_spn_cycles.append(spn_cycles);

	QSpinBox *spn_priority = new QSpinBox(this);
	spn_priority->move(180, this->Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	spn_priority->setFixedSize(60, this->COMPONENT_HEIGHT);
	spn_priority->setObjectName(QString("spn_p_") + (65 + this->preliminary_process_count));
	spn_priority->show();
	lst_spn_priorities.append(spn_priority);

	QProgressBar *pbr_status = new QProgressBar(this);
	pbr_status->move(260, this->Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	pbr_status->setFixedSize(this->PROGRESSBAR_WIDTH, this->COMPONENT_HEIGHT);
	pbr_status->setObjectName(QString("pbr_") + (65 + this->preliminary_process_count));
	pbr_status->setRange(0, 1);
	pbr_status->show();
	lst_pbr_status.append(pbr_status);

	QPushButton *btn_activate = new QPushButton(this);
	btn_activate->move(890, this->Y_OFFSET + (this->Y_PADDING * this->preliminary_process_count));
	btn_activate->setFixedSize(30, this->COMPONENT_HEIGHT);
	connect(btn_activate, SIGNAL(clicked()), this, SLOT(activate_process()));
	btn_activate->setObjectName(QString("btn_") + (65 + this->preliminary_process_count));
	btn_activate->show();
	lst_btn_activate.append(btn_activate);

	++this->preliminary_process_count;

	this->refresh_bottom_buttons();
}

/**
 * @brief ProcessesOverview::add_queue is similar to @see add_process() but for the MQ section. Adding input rows for queues in the case of MQ
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::add_queue()
{
	QLabel *lbl_queue = new QLabel(grp_mq);
	lbl_queue->move(30, this->MQ_Y_OFFSET + (this->Y_PADDING * this->preliminary_queue_count));
	lbl_queue->setFixedSize(60, this->COMPONENT_HEIGHT);
	QString title = QString("Queue ") + (49 + this->preliminary_queue_count);
	lbl_queue->setText(title);
	lbl_queue->show();
	lst_lbl_mq.append(lbl_queue);

	QSpinBox *spn_tq = new QSpinBox(grp_mq);
	spn_tq->move(110, this->MQ_Y_OFFSET + (this->Y_PADDING * this->preliminary_queue_count));
	spn_tq->setFixedSize(60, this->COMPONENT_HEIGHT);
	spn_tq->setObjectName(QString("spn_tq_") + (49 + this->preliminary_queue_count));
	spn_tq->show();
	spn_tq->setMinimum(1);
	lst_spn_mq.append(spn_tq);

	++this->preliminary_queue_count;

	this->refresh_mq_buttons();

	if (this->preliminary_queue_count == 9)
		btn_add_queue->setEnabled(false);
}

/**
 * @brief ProcessesOverview::activate_process happens when the activation buttons to the right of each process is clicked in order to enqueue the process for execution (or to be placed in the READY pool)
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::activate_process()
{
	QObject *senderObj = sender();
	((QPushButton*)senderObj)->setEnabled(false);
	change_button_color(((QPushButton*)senderObj), Qt::green);

	QString process_id = (((QPushButton*)senderObj)->objectName()).mid(4);

	QSpinBox *spn = this->findChild<QSpinBox*>(QString("spn_") + process_id);
	spn->setEnabled(false);

	QSpinBox *spn_p = this->findChild<QSpinBox*>(QString("spn_p_") + process_id);

	QProgressBar *pbr = this->findChild<QProgressBar*>(QString("pbr_" + process_id));
	pbr->setRange(0, spn->value() * this->CYCLES_FACTOR);

	Process *new_process = new Process(process_id.at(0).unicode(), spn->value() * this->CYCLES_FACTOR, spn_p->value());

	this->get_active_processes()->enqueue_process(new_process);
}

/**
 * @brief ProcessesOverview::submit_mq submits the input values to avoid altering while simulation is active
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::submit_mq()
{
	btn_add_queue->setEnabled(false);
	btn_submit_mq->setEnabled(false);

	for (QSpinBox* spn : lst_spn_mq) {
		spn->setEnabled(false);
		this->multiple_queues.append(new ProcessQueue(spn->value(), spn->objectName().mid(7).at(0).unicode()));
	}

//	this->enable_main();
}

/**
 * @brief ProcessesOverview::change_button_color changes the color of the activation buttons (or any other -> the function is generic) to indicate process status
 * @param button to change
 * @param color to paint the button provided in the other parameter
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::change_button_color(QPushButton* button, Qt::GlobalColor color)
{
	QPalette pal = button->palette();
	pal.setColor(QPalette::Button, QColor(color));
	button->setAutoFillBackground(true);
	button->setPalette(pal);
	button->update();
}

/**
 * @brief ProcessesOverview::reset_button_color resets the given button's color to the original
 * @param button to reset color
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::reset_button_color(QPushButton* button)
{
	QPalette pal = button->palette();
	QColor color = btn_exit->palette().color(QPalette::Button);
	pal.setColor(QPalette::Button, QColor(color));
	button->setAutoFillBackground(true);
	button->setPalette(pal);
	button->update();
}

/**
 * @brief ProcessesOverview::update_progressbar updates the value of the progressbar corresponding to the progressed process
 * @param p_id id of process which executed or made progress
 * @param progress of the process given in the other parameter
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::update_progressbar(unsigned short p_id, unsigned int progress)
{
	QProgressBar *pbr = this->findChild<QProgressBar*>(QString("pbr_") + p_id);
	pbr->setValue(progress);
}

/**
 * @brief ProcessesOverview::start starts the simulation which starts a timer corresponding to the chosen algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::start()
{
	unsigned short scheduler = cmb_algorithm->currentIndex();
	btn_pause->setEnabled(true);
	btn_start->setEnabled(false);
	btn_reset->setEnabled(false);
	cmb_algorithm->setEnabled(false);

	switch (scheduler)
	{
		case 0: //FCFS
			this->process_iterator = this->get_active_processes()->get_first();
			tmr_fcfs->start();
			break;
		case 1: //SJF
			this->process_iterator = this->find_shortest_remaining_time();
			tmr_sjf->start();
			break;
		case 2: //SRT
			tmr_srt->start();
			break;
		case 3: //RR
			this->process_iterator = this->get_active_processes()->get_first();
			tmr_rr->start();
			break;
		case 4: //PR
			this->process_iterator = this->find_highest_priority();
			tmr_pr->start();
			break;
		case 5: //MQ
//			this->process_iterator = this->get_active_processes()->get_first();
			if (!btn_submit_mq->isEnabled())
				tmr_mq->start();
			break;
	}
}

/**
 * @brief ProcessesOverview::fcfs is the implementation of the First Come First Serve scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::fcfs()
{
//	for (Process *p : this->active_processes) {
//		while (!p->is_completed()) {
//			p->tick();
//			update_progressbar(p->get_id(), p->get_cycles_completed());
//			QThread::sleep(1);
//		}
//		finish_process(p);
//	}

	if (!this->get_active_processes()->is_empty()) {
		this->process_iterator->tick();
		this->update_progressbar(this->process_iterator->get_id(), this->process_iterator->get_cycles_completed());
		if (this->process_iterator->is_completed()) {
			this->finish_process(this->process_iterator);
			if (!this->get_active_processes()->is_empty())
				this->process_iterator = this->get_active_processes()->get_first();
		}
	} else {
		tmr_fcfs->stop();
		done();
	}
}

/**
 * @brief ProcessesOverview::sjf is the implementation of the Shortest Job First scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::sjf()
{
//	while (!this->active_processes.isEmpty()) {
//		Process *p = find_shortest_remaining_time();
//		while (!p->is_completed()) {
//			p->tick();
//			this->update_progressbar(p->get_id(), p->get_cycles_completed());
//			QThread::sleep(1);
//		}
//		finish_process(p);
//	}

	if (!this->get_active_processes()->is_empty()) {
		this->process_iterator->tick();
		this->update_progressbar(this->process_iterator->get_id(), this->process_iterator->get_cycles_completed());
		if (this->process_iterator->is_completed()) {
			this->finish_process(this->process_iterator);
			if (!this->get_active_processes()->is_empty())
				this->process_iterator = this->find_shortest_remaining_time();
		}
	} else {
		tmr_sjf->stop();
		done();
	}
}

/**
 * @brief ProcessesOverview::srt is the implementation of the Shortest Remaining Time scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::srt()
{
//	while (!this->active_processes.isEmpty()) {
//		Process *p = find_shortest_remaining_time();
//		p->tick();
//		update_progressbar(p->get_id(), p->get_cycles_completed());
//		QThread::sleep(1);
//		if (p->is_completed())
//			finish_process(p);
//	}

	if (!this->get_active_processes()->is_empty()) {
		this->process_iterator = this->find_shortest_remaining_time();
		this->process_iterator->tick();
		this->update_progressbar(this->process_iterator->get_id(), this->process_iterator->get_cycles_completed());
		if (this->process_iterator->is_completed())
			this->finish_process(this->process_iterator);
	} else {
		tmr_srt->stop();
		done();
	}
}

/**
 * @brief ProcessesOverview::rr is the implementation of the Round Robin scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::rr()
{
//	while (!this->active_processes.isEmpty())
//		for (Process *p : this->active_processes)
//			if (!p->is_completed()) {
//				p->tick();
//				update_progressbar(p->get_id(), p->get_cycles_completed());
//				QThread::sleep(1);
//				if (p->is_completed())
//					finish_process(p);
//			}
	Process *temp_next;
	if (!this->get_active_processes()->is_empty()) {
		this->process_iterator->tick();
		this->update_progressbar(this->process_iterator->get_id(), this->process_iterator->get_cycles_completed());

		if (!this->process_iterator->is_completed() && this->get_active_processes()->get_process_count() > 1)
					this->process_iterator = this->process_iterator->get_next_process();
		else if (this->get_active_processes()->get_process_count() > 1 && this->process_iterator->is_completed()) {
			temp_next = this->process_iterator->get_next_process();
			this->finish_process(this->process_iterator);
			this->process_iterator = temp_next;
		} else if (this->process_iterator->is_completed() && this->get_active_processes()->get_process_count() < 2)
			this->finish_process(this->process_iterator);
	} else {
		tmr_rr->stop();
		done();
	}
}

/**
 * @brief ProcessesOverview::pr is the implementation of the Priority scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::pr()
{
	if (!this->get_active_processes()->is_empty()) {
		this->process_iterator = this->find_highest_priority();
		this->process_iterator->tick();
		this->update_progressbar(this->process_iterator->get_id(), this->process_iterator->get_cycles_completed());
		if (this->process_iterator->is_completed())
			this->finish_process(this->process_iterator);
	} else {
		tmr_pr->stop();
		done();
	}
}

/**
 * @brief ProcessesOverview::mq is the implementation of the Multiple Queues scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::mq()
{
	// Monitor incomming processes
	if (!this->get_active_processes()->is_empty()) {
		unsigned short i;
		Process *p;
		for (i = 0, p = this->get_active_processes()->get_first(); i < this->get_active_processes()->get_process_count(); ++i, p = p->get_next_process()) {
			this->get_multiple_queues().at(0)->enqueue_process(p);
			this->get_active_processes()->evolve_process(p);
		}
	}

	unsigned short i;
	ProcessQueue *pq;
	for (i = 0, pq = this->get_multiple_queues().at(i); i < this->get_multiple_queues().count(); ++i, pq = this->get_multiple_queues().at(i)) { //Finds the first queue with at least one process
		if (pq->get_process_count() > 0) {
			for (unsigned short ii = 0; ii < pq->get_time_quantum(); ++ii) {
				pq->get_first()->tick();
				this->update_progressbar(pq->get_first()->get_id(), pq->get_first()->get_cycles_completed());
//				std::cout << "going to sleep" << std::endl;
//				tmr_mq->thread()->sleep(1);
//				tmr_mq->thread()->wait(1000);
				// Start new timer? //TODO: research timer with parameter
				if (pq->get_first()->is_completed()) {
					pq->remove_process(pq->get_first());
					break;
				} else if (ii == pq->get_time_quantum() - 1) {
					if (i == this->get_multiple_queues().count() - 1) {
						this->get_multiple_queues().at(i)->enqueue_process(pq->get_first());
						this->get_multiple_queues().at(i)->evolve_process(pq->get_first()); //TODO: Test this, there will be a duplicaye process in queue for a brief moment, otherwise make temp Process pointer followed by calling evolve followed by enqueue
					} else {
						this->get_multiple_queues().at(i + 1)->enqueue_process(pq->get_first());
						this->get_multiple_queues().at(i)->evolve_process(pq->get_first());
					}
				}
			}
			break;
		}
	}

	bool all_empty = true;
	for (unsigned short iii = 0; iii < this->get_multiple_queues().count() && all_empty; ++iii)
		if (!this->get_multiple_queues().at(iii)->is_empty())
			all_empty = false;
	if (all_empty) {
		tmr_mq->stop();
		done();
	}
}

void ProcessesOverview::mqq()
{

}

/**
 * @brief ProcessesOverview::find_shortest_remaining_time finds the process with the shortest remaining time
 * Used in both SJF and SRT algorithms
 * @return the process with the shortest remaining time
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process* ProcessesOverview::find_shortest_remaining_time()
{
	Process *shortest_p, *p;
	unsigned short i;
	for (shortest_p = this->get_active_processes()->get_first(), p = this->get_active_processes()->get_first(), i = 0; i < this->get_active_processes()->get_process_count(); ++i, p = p->get_next_process())
		if (p->get_cycles_remaining() < shortest_p->get_cycles_remaining())
			shortest_p = p;

	return shortest_p;
}

/**
 * @brief ProcessesOverview::find_highest_priority finds the process with the highest priority
 * Used in priority scheduling only at this stage
 * @return the process with the highest priority
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process* ProcessesOverview::find_highest_priority()
{
	Process *highest_p, *p;
	unsigned short i;
	for (highest_p = this->get_active_processes()->get_first(), p = this->get_active_processes()->get_first(), i = 0; i < this->get_active_processes()->get_process_count(); ++i, p = p->get_next_process())
		if (p->get_priority() < highest_p->get_priority())
			highest_p = p;

	return highest_p;
}

/**
 * @brief ProcessesOverview::finish_process Removes a process from the READY or RUNNING pool when it's done
 * @param p process to remove
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::finish_process(Process *p)
{
	QPushButton *btn = this->findChild<QPushButton*>(QString("btn_") + p->get_id());
	this->get_active_processes()->remove_process(p);
	this->change_button_color(btn, Qt::blue);
}

/**
 * @brief ProcessesOverview::pause pauses the simulation to provide some time to add processes, however processes can be added in real time as well
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::pause()
{
	btn_pause->setEnabled(false);
	btn_start->setEnabled(true);
	btn_start->setText("Continue");
	btn_reset->setEnabled(true);
	cmb_algorithm->setEnabled(true);
	if (tmr_fcfs->isActive())
		tmr_fcfs->stop();
	else if (tmr_sjf->isActive())
		tmr_sjf->stop();
	else if (tmr_srt->isActive())
		tmr_srt->stop();
	else if (tmr_rr->isActive())
		tmr_rr->stop();
	else if (tmr_pr->isActive())
		tmr_pr->stop();
	else if (tmr_mq->isActive())
		tmr_mq->stop();
}

/**
 * @brief ProcessesOverview::reset resets the simulation, keeping the previous values, but ready to run again, processes needs to activated again to enter the READY pool
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::reset()
{
	for (QPushButton *btn : this->lst_btn_activate) {
		btn->setEnabled(true);
		this->reset_button_color(btn);
	}

	for (QProgressBar *pbr : this->lst_pbr_status)
		pbr->setValue(0);

	for (QSpinBox *spn : this->lst_spn_cycles)
		spn->setEnabled(true);

	btn_reset->setEnabled(false);
	btn_start->setEnabled(true);
	btn_start->setText("Start");
	btn_pause->setEnabled(false);
	cmb_algorithm->setEnabled(true);

	this->get_active_processes()->~ProcessQueue();
}

/**
 * @brief ProcessesOverview::done executes when all processes are done executing and READY pool((all) process queue(s)) is empty.
 * Sets GUI up for another run
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::done()
{
	btn_pause->setEnabled(false);
	btn_start->setText("Start");
	btn_reset->setEnabled(true);
	cmb_algorithm->setEnabled(true);
	this->get_active_processes()->~ProcessQueue();
	this->get_multiple_queues().clear();
}

/**
 * @brief ProcessesOverview::exit Exits the application/program
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessesOverview::exit()
{
	QCoreApplication::quit();
}

/**
 * @brief ProcessesOverview::get_active_processes accessor for the READY pool
 * @return the queue of processes in the READY pool
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessQueue* ProcessesOverview::get_active_processes()
{
	return this->active_processes;
}

/**
 * @brief ProcessesOverview::get_multiple_queues accessor for multiple queues
 * @return the queue of queues houding all processes for the Multiple Queues scheduling algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
QList<ProcessQueue*> ProcessesOverview::get_multiple_queues()
{
	return this->multiple_queues;
}

/**
 * @brief ProcessesOverview::~ProcessesOverview default destructor
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessesOverview::~ProcessesOverview()
{
	delete this->get_active_processes();
}
