/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 */

#ifndef PROCESSESOVERVIEW_H
#define PROCESSESOVERVIEW_H

#include "processqueue.hpp"

#include <QList>

#include <QDesktopWidget>
#include <QMainWindow>
#include <QComboBox>
#include <QLabel>
#include <QSpinBox>
#include <QProgressBar>
#include <QPushButton>
#include <QGroupBox>

#include <QTimer>

/**
 * @brief The ProcessesOverview class
 * ITRW 316
 * processoverview.hpp
 * Purpose: House the main GUI of the application and its functionality
 */
class ProcessesOverview : public QMainWindow
{
    Q_OBJECT

public:
	ProcessesOverview(QWidget *parent = 0);
	~ProcessesOverview();

private:
	// CONSTANTS
#ifdef __APPLE__
	const QDesktopWidget WIDGET;
	const QRect SCREEN_RESOLUTION = WIDGET.availableGeometry(WIDGET.primaryScreen()); // or screenGeometry(), depending on your needs
	const unsigned short SCREEN_WIDTH = SCREEN_RESOLUTION.width();
	const unsigned short SCREEN_HEIGHT = SCREEN_RESOLUTION.height();
	const unsigned int Y_OFFSET = 120;
	const unsigned int MQ_Y_OFFSET = 60;
	const unsigned short Y_PADDING = 30;
	const unsigned short COMPONENT_HEIGHT = 25;
	const unsigned short BOTTOM_BUTTON_WIDTH = 130;
	const unsigned short BOTTOM_BUTTON_HEIGHT = 35;
	const unsigned short PROGRESSBAR_WIDTH = 620;
	const unsigned short BOTTOM_BUTTONS_Y_OFFSET = 180;
#elif
	const QDesktopWidget WIDGET;
	const QRect SCREEN_RESOLUTION = WIDGET.availableGeometry(WIDGET.primaryScreen()); // or screenGeometry(), depending on your needs
	const unsigned short SCREEN_WIDTH = SCREEN_RESOLUTION.width();
	const unsigned short SCREEN_HEIGHT = SCREEN_RESOLUTION.height();
	const unsigned int Y_OFFSET = 120;
	const unsigned int MQ_Y_OFFSET = 60;
	const unsigned short Y_PADDING = 30;
	const unsigned short COMPONENT_HEIGHT = 25;
	const unsigned short BOTTOM_BUTTON_WIDTH = 120;
	const unsigned short BOTTOM_BUTTON_HEIGHT = 30;
	const unsigned short PROGRESSBAR_WIDTH = 620;
	const unsigned short BOTTOM_BUTTONS_Y_OFFSET = 180;
#endif
	// Below two lines are in favor of beauty and smoothness (timers abd cycles on steroids)
	const unsigned int CYCLES_FACTOR = 100;
	const unsigned short TIMER_INTERVAL = 5;
	// Below two lines are in favor of accuracy and close observation (1 second per cycle)
//	const unsigned int CYCLES_FACTOR = 1;
//	const unsigned short TIMER_INTERVAL = 1000;

	// global variables
	unsigned short preliminary_process_count; // for GUI manipulation purposes only
	unsigned short preliminary_queue_count; // for GUI manipulation purposes only
	ProcessQueue *active_processes; // All processes are placed in a doubly linked circular queue for easier hyperthreading and switching
	Process *process_iterator;
	QList<ProcessQueue*> multiple_queues;

	// GUI
	QComboBox *cmb_algorithm;
	QLabel *lbl_cycles;
	QLabel *lbl_priority;
	QList<QLabel*> lst_lbl_processes;
	QList<QSpinBox*> lst_spn_cycles;
	QList<QSpinBox*> lst_spn_priorities;
	QList<QProgressBar*> lst_pbr_status;
	QList<QPushButton*> lst_btn_activate;
	QPushButton *btn_add_process;
	QPushButton *btn_start;
	QPushButton *btn_pause;
	QPushButton *btn_reset;
	QPushButton *btn_exit;
	// MQ GUI Section
	QGroupBox *grp_mq;
	QLabel *lbl_tq;
	QList<QLabel*> lst_lbl_mq;
	QList<QSpinBox*> lst_spn_mq;
	QPushButton *btn_add_queue;
	QPushButton *btn_submit_mq;

	// Timers
	QTimer *tmr_fcfs;
	QTimer *tmr_sjf;
	QTimer *tmr_srt;
	QTimer *tmr_rr;
	QTimer *tmr_pr;
	QTimer *tmr_mq;
	QTimer *tmr_mqq;

	// Accessors
	ProcessQueue* get_active_processes();
	QList<ProcessQueue*> get_multiple_queues();

	// GUI functions
	void refresh_bottom_buttons();
	void refresh_mq_buttons();
	void change_button_color(QPushButton*, Qt::GlobalColor);
	void reset_button_color(QPushButton*);
	void update_progressbar(unsigned short, unsigned int);

	// helper functions
	Process* find_shortest_remaining_time();
	Process* find_highest_priority();
	void finish_process(Process *p);

	// functions to help logical control of application
	void done();

private slots:
	// Scheduling algorithms
	void fcfs();
	void sjf();
	void srt();
	void rr();
	void pr();
	void mq();
	void mqq();

	// Button and other GUI signal handlers (events)
	void add_process();
	void add_queue();
	void exit();
	void activate_process();
	void submit_mq();
	void start();
	void reset();
	void pause();
	void algorithm_change(const QString&);
};

#endif // PROCESSESOVERVIEW_H
