/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 *
 * Purpose: Implementation of the ProcessQueue class declared in processqueue.hpp. @see processqueue.hpp
 * ITRW 316
 * processqueue.cpp
 */

#include "processqueue.hpp"

//#include <iostream> // Used in testing only

/**
 * @brief ProcessQueue::ProcessQueue the default constructor
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessQueue::ProcessQueue()
{
	this->first = this->last = 0;
	this->process_count = 0;
	this->set_time_quantum(1);
}

/**
 * @brief ProcessQueue::ProcessQueue another overloaded constructor
 * @param tq the queue with a given process time quantum
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessQueue::ProcessQueue(unsigned short id, unsigned int tq)
{
	ProcessQueue();
	this->id = id;
	this->set_time_quantum(tq);
}

/**
 * @brief ProcessQueue::ProcessQueue the overloaded constructor
 * @param p initaites the queue with a given process p
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessQueue::ProcessQueue(Process *p, unsigned int tq)
{
	ProcessQueue();
	this->set_time_quantum(tq);
	this->enqueue_process(p);
}

/**
 * @brief ProcessQueue::get_first
 * @return the process front/first in queue
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process* ProcessQueue::get_first()
{
	return this->first;
}

/**
 * @brief ProcessQueue::get_last
 * @return the last process in queue
 * @since 1.0
 * @version 1.0 03/03/19
 */
Process* ProcessQueue::get_last()
{
	return this->last;
}

/**
 * @brief ProcessQueue::is_empty
 * @return true if the queue is empty
 * @since 1.0
 * @version 1.0 03/03/19
 */
bool ProcessQueue::is_empty()
{
	return get_first() == 0;
}

/**
 * @brief ProcessQueue::get_time_quantum
 * @return the time quantum of this process queue
 * used in Multiple Queues and Round Robin algorithm
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned int ProcessQueue::get_time_quantum()
{
	return this->tq;
}

/**
 * @brief ProcessQueue::get_process_count
 * @return the number of processes in the queue
 * Very useful as this is a circular queue
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned short ProcessQueue::get_process_count()
{
	return this->process_count;
}

/**
 * @brief ProcessQueue::get_id accessor to queue id
 * @return the id of this queue
 * Helpfull in the case of Multiple Queues scheduling
 * @since 1.0
 * @version 1.0 03/03/19
 */
unsigned short ProcessQueue::get_id()
{
	return this->id;
}

/**
 * @brief ProcessQueue::set_time_quantum
 * @param tq time quantum for this queue
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessQueue::set_time_quantum(unsigned int tq)
{
	this->tq = tq;
}

/**
 * @brief ProcessQueue::enqueue_process
 * @param p process to enter this queue
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessQueue::enqueue_process(Process *p)
{
	if (!this->is_empty()) {
		this->get_last()->set_next_process(p);
		p->set_previous_process(this->get_last());
		this->last = this->get_last()->get_next_process();
	} else
		this->first = this->last = p;

	// Doubly linked circular queue
	this->get_last()->set_next_process(this->get_first());
	this->get_first()->set_previous_process(this->get_last());
	++this->process_count;
}

/**
 * @brief ProcessQueue::remove_process
 * @param p process to remove from queue
 * @since 1.0
 * @version 1.0 03/03/19
 */
void ProcessQueue::remove_process(Process *p)
{
	if (!this->is_empty()) {
		// if only one process existsin the queue  -- nuke it
		if (this->get_first() == this->get_last() && p == this->get_first()) {
			delete this->get_first();
//			delete p;
			this->first = this->last = 0;
		// if the first process is to be deleted
		} else if (p->get_id() == this->get_first()->get_id()) {
			this->first = this->get_first()->get_next_process();
			this->get_last()->set_next_process(this->get_first());
			this->get_first()->set_previous_process(this->get_last());
			delete p;
		} else {
			Process *pred, *temp;
			unsigned short i;
			for (pred = this->get_first(), temp = this->get_first()->get_next_process(), i = 0; i < this->get_process_count() && temp != p; pred = pred->get_next_process(), temp = temp->get_next_process(), ++i);
			// if a process is actually found in the above line, then and only then we nuke
			if (temp != 0) {
				pred->set_next_process(temp->get_next_process());
				temp->get_next_process()->set_previous_process(pred);
				if (temp == this->get_last())
					this->last = pred;
				delete p;
			}
		}
//		this->eveolve_process(p);
//		delete p;
		--this->process_count;
	}
}

void ProcessQueue::evolve_process(Process *p)
{
	if (!this->is_empty()) {
		// if only one process existsin the queue  -- nuke it
		if (this->get_first() == this->get_last() && p == this->get_first()) {
//			delete this->get_first();
			this->first = this->last = 0;
		// if the first process is to be deleted
		} else if (p->get_id() == this->get_first()->get_id()) {
			this->first = this->get_first()->get_next_process();
			this->get_last()->set_next_process(this->get_first());
			this->get_first()->set_previous_process(this->get_last());
//			delete p;
		} else {
			Process *pred, *temp;
			unsigned short i;
			for (pred = this->get_first(), temp = this->get_first()->get_next_process(), i = 0; i < this->get_process_count() && temp != p; pred = pred->get_next_process(), temp = temp->get_next_process(), ++i);
			// if a process is actually found in the above line, then and only then we nuke
			if (temp != 0) {
				pred->set_next_process(temp->get_next_process());
				temp->get_next_process()->set_previous_process(pred);
				if (temp == this->get_last())
					this->last = pred;
//				delete p;
			}
		}
		--this->process_count;
	}
}

/**
 * @brief ProcessQueue::list_processes
 * Only used in testing
 * @since 1.0
 * @version 1.0 03/03/19
 */
//void ProcessQueue::list_processes()
//{
//	if (this->is_empty())
//		std::cerr << "Process Queue Empty" << std::endl;
//	else {
//		std::cout << "A total of " << this->get_process_count() << " processes" << std::endl;
//		int i = 0;
//		for (Process *p = this->get_first(); i < this->get_process_count() ; p = p->get_next_process(), ++i)
//			std::cout << "Process: " << (char)p->get_id() << " Cycles completed: " << p->get_cycles_completed() << " Length: " << p->get_cycles_total() << std::endl;
//	}
//}

/**
 * @brief ProcessQueue::~ProcessQueue default destructor to delete all processes from memory (if any) from this queue BEFORE this queue is deleted from memory.
 * We don't want memory leaks like the ones in Windows.
 * @since 1.0
 * @version 1.0 03/03/19
 */
ProcessQueue::~ProcessQueue()
{
	for (Process *p; !this->is_empty();) {
		p = this->get_first();
		this->remove_process(p);
	}
	this->process_count = 0;
	this->set_time_quantum(1);
}
