/**
 * Copyright (C) 2019 Zander Labuschagne. All rights reserved.
 * @version 1.0 03/03/19
 * @since 1.0
 *
 * Authors:
 *         @author Zander Labuschagne <zander.labuschagne@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the MIT License, as published by the
 * Massachusetts Institute of Technology.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with this program; if not, write to the author,
 * contact details above in line 7.
 */

#ifndef PROCESSQUEUE_HPP
#define PROCESSQUEUE_HPP

#include "process.hpp"

/**
 * @brief The ProcessQueue class
 * ITRW 316
 * processqueue.hpp
 * Purpose: House all processes in an orderly manner. This results in easy management of processes in the Running or Ready pool.
 * Contains the following members along with their accessor and mutator functions where necesarry:
 * first: Pointer/reference to the process in the front of the queue.
 * last: Pointer/reference to the process in the back of the queue.
 * id: to identify the queue if more than one is used like in the case of Multiple Queues scheduling
 * tq: Denotes the time quantum of this queue. Only used in the Multiple Queues and Round Robin scheduling algorithms. The default is 1 for use in plain original Round Robin.
 * process_count: Denoting the number of processes in this queue. Very helpful as the is a circular queue.
 */
class ProcessQueue
{
public:
	ProcessQueue();
	ProcessQueue(unsigned short id, unsigned int tq);
	ProcessQueue(Process *p, unsigned int tq = 1);
	~ProcessQueue();
	Process* get_first();
	Process* get_last();
	unsigned int get_time_quantum();
	unsigned short get_process_count();
	unsigned short get_id();
	bool is_empty();
	void set_time_quantum(unsigned int tq);
	void enqueue_process(Process* p);
	void remove_process(Process* p);
	void evolve_process(Process* p);
	void list_processes();

private:
	Process *first;
	Process *last;
	unsigned short id;
	unsigned int tq;
	unsigned short process_count;
};

#endif // PROCESSQUEUE_HPP
